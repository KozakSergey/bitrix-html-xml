# Bitrix модуль по генерации html карты сайта из xml и наоборот

Данный модуль генерирует html карту из xml, а так же есть возможность генерировать xml карту из html карты. 
Задача не распространенная, но так как такие запросы встречались было решено реализовать модуль для выполнения такого функционала. 
Модуль находится в стадии доработки и оптимизации, тем не менее выполняет изначально запланированный функционал. 
Достаточно установить папку sksp.module в директорию /bitrix/modules/ или /local/modules/ через ftp, а затем установить из маркетплейса решение. Выполнить настройки в админке, там все интуитивно понятно.