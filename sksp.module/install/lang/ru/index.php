<?php
/*
 * Файл local/modules/scrollup/lang/ru/install/index.php
 */

$MESS['ORWO_SKSP_MODULE_NAME']      = 'Генерация карт сайта (HTML и XML) - SKSP (ORWO)';
$MESS['ORWO_SKSP_MODULE_DESC']      = 'После установки вы сможете пользоваться компонентом orwo:html_sitemap';
$MESS['SCROLLUP_DESCRIPTION']       = 'Добавляет на сайт кнопку плавной прокрутки наверх.';
$MESS['ORWO_SKSP_INSTALL_TITLE']      = 'Установка модуля sksp.module';
$MESS['ORWO_SKSP_UNINSTALL_TITLE']      = 'Деинсталляция модуля sksp_module';