<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); 
if(!CModule::IncludeModule("iblock"))
    return;
$arComponentDescription = array(
    "NAME" => GetMessage("ORWO_SMP_COMPONENT_NAME"),
    "DESCRIPTION" => GetMessage("ORWO_SMP_COMPONENT_DESCRIPTION"),
    "PATH" => array(
        'NAME' => GetMessage("ORWO_SMP_PATH_NAME"),
        "ID" => "orwocompany",
        "CHILD" => array(
            "ID" => "htmlsitemaporwo",
            "NAME" => GetMessage("ORWO_SMP_PATH_CHILD_NAME"),
        ),
    ),
);
?>