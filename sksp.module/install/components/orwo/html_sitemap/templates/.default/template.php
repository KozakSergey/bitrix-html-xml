<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
      /** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
//echo var_dump($arResult);
?><ul>
<?foreach($arResult["ITEM_SECT"] as $arSection){?>
      <li class='level_<?=$arSection["DEPTH_LEVEL"]?>'><a href='<?=$arSection["SECTION_PAGE_URL"]?>'><?=$arSection["NAME"]?></a></li>
<ul>
<?foreach($arSection["ELEMS"] as $arElem){?>
      <li class='sitemap_item'><a href='<?=$arElem["DETAIL_PAGE_URL"]?>'><?=$arElem['NAME']?></a></li>
<?}?>     
</ul>
<?}?>
<?foreach($arResult["ITEM_ELEMS"] as $arDirElem){?>
      <li class='level_1'><a href="<?=$arDirElem["DETAIL_PAGE_URL"]?>"><?=$arDirElem["NAME"]?></a></li>
<?}?>
</ul>
<style type="text/css">
      .level_1{
margin-left: 10px;
}
.level_2{
margin-left: 40px;
}
.level_3{
margin-left: 60px;
}
.sitemap_item{
margin-left: 80px;
}
</style>