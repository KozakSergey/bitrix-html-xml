<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Main\Loader;
use Bitrix\Iblock\Template;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Page\Asset;
IncludeTemplateLangFile(__FILE__);

if($arParams["IBLOCK_ID"]=="" ||$arParams["IBLOCK_ID"][0]==""){
  echo GetMessage("ORWO_SMP_NOT_IBLOCK");
}else{
\Bitrix\Main\Loader::IncludeModule('iblock');
//echo var_dump($arParams);

$arSelect = [
  'ID',
  'LEFT_MARGIN',
  'DEPTH_LEVEL',
  'NAME',
  'CODE',
  'SECTION_PAGE_URL'
];
$arOrder = [
  'LEFT_MARGIN' => $arParams["LEFT_MARGIN"]
];
$arFilter = [
  'IBLOCK_ID' => $arParams["IBLOCK_ID"],
  'ACTIVE'    => "Y"
];
$resSections = \CIBlockSection::GetList($arOrder, $arFilter, false, $arSelect);
$resSections->SetUrlTemplates($arParams["SECTION_PAGE_URL"]);
$i=0;
$sectAvaliable = false;
while($arSect = $resSections->GetNext()){
  $sectAvaliable = true;
	$dbElements = CIBlockElement::GetList(
     	 array("SORT"=>"ASC"),
         array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "SECTION_ID" => $arSect['ID'], "ACTIVE" => "Y"),
         false,
         false,
         array("ID", "IBLOCK_ID", "NAME", "CODE","DETAIL_PAGE_URL")
     );
	$arResult["ITEM_SECT"][] = $arSect;
  $dbElements->SetUrlTemplates($arParams["DETAIL_PAGE_URL"]);
	while($arElem = $dbElements->GetNext()){
		$arResult["ITEM_SECT"][$i]["ELEMS"][]=$arElem;
	}
	$i++;
}

  $dbElements = CIBlockElement::GetList(
       array("SORT"=>"ASC"),
         array("IBLOCK_ID" => $arParams["IBLOCK_ID"], "SECTION_ID" => false, "ACTIVE" => "Y"),
         false,
         false,
         array("ID", "IBLOCK_ID", "NAME", "CODE","DETAIL_PAGE_URL")
     );
  $dbElements->SetUrlTemplates($arParams["DETAIL_PAGE_URL"]);
  while($arElem = $dbElements->GetNext()){
    $arResult["ITEM_ELEMS"][]=$arElem;
  }
if($this->startResultCache(false, array(($arParams["CACHE_GROUPS"]==="N" ?
false: $USER->GetGroups()))))

{

   if(!Loader::includeModule("iblock"))

   {

      $this->abortResultCache();

      ShowError(GetMessage("ORWO_SMP_NOT_IBLOCK_MODULE"));

      return;

   }  
if(isset($arResult["ITEM_SECT"])){   
usort($arResult["ITEM_SECT"], function($a, $b){
  if ($a["IBLOCK_ID"] == $b["IBLOCK_ID"]) {
      return 0;
  }
  return ($a["IBLOCK_ID"] < $b["IBLOCK_ID"]) ? -1 : 1;
});
}
if(isset($arResult["ITEM_ELEMS"])){
usort($arResult["ITEM_ELEMS"], function($a, $b){
  if ($a["IBLOCK_ID"] == $b["IBLOCK_ID"]) {
      return 0;
  }
  return ($a["IBLOCK_ID"] < $b["IBLOCK_ID"]) ? -1 : 1;
});
}
$this->IncludeComponentTemplate();
}
}
?>