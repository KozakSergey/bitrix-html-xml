<?
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Config\Option;
use Bitrix\Highloadblock\HighloadBlockTable;

Class sksp_module extends CModule
{
	var $MODULE_ID = "sksp.module";
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	var $MODULE_CSS;
	function __construct()
	{
		$arModuleVersion = array();
		$path = str_replace("\\", "/", __FILE__);
		$path = substr($path, 0, strlen($path) - strlen("/index.php"));
		include($path."/version.php");
		if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion))
		{
			$this->MODULE_VERSION = $arModuleVersion["VERSION"];
			$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		}
		$this->MODULE_NAME = GetMessage("ORWO_SKSP_MODULE_NAME");
		$this->MODULE_DESCRIPTION = GetMessage("ORWO_SKSP_MODULE_DESC");
	}
	function InstallFiles()
	{
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/local/modules/sksp.module/install/components",
		             $_SERVER["DOCUMENT_ROOT"]."/bitrix/components", true, true);
		return true;
	}
	function UnInstallFiles()
	{
		DeleteDirFilesEx("/local/components/orwo");
		return true;
	}
	function DoInstall()
	{
		global $DOCUMENT_ROOT, $APPLICATION;
		$this->InstallFiles();
		ModuleManager::registerModule("sksp.module");
		$APPLICATION->IncludeAdminFile(GetMessage("ORWO_SKSP_INSTALL_TITLE"), $DOCUMENT_ROOT."/local/modules/sksp.module/install/step.php");
	}
	function DoUninstall()
	{
		global $DOCUMENT_ROOT, $APPLICATION;
		$this->UnInstallFiles();
        \CAgent::RemoveModuleAgents("sksp.module");
		ModuleManager::unRegisterModule("sksp.module");
		$APPLICATION->IncludeAdminFile(GetMessage("ORWO_SKSP_UNINSTALL_TITLE"), $DOCUMENT_ROOT."/local/modules/sksp.module/install/unstep.php");
	}
}
?>