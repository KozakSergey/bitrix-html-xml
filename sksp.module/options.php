<?php

use Sksp\SitemapParserOrwo\SitemapParser;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\HttpApplication;
use Bitrix\Main\Loader;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Application;
Loc::loadMessages(__FILE__);

// получаем идентификатор модуля
$request = HttpApplication::getInstance()->getContext()->getRequest();
$module_id = htmlspecialchars($request['mid'] != '' ? $request['mid'] : $request['id']);
// подключаем наш модуль
Loader::includeModule($module_id);

/*
 * Параметры модуля со значениями по умолчанию
 */
$aTabs = array(
    array(
        /*
         * Первая вкладка «Настройки генерации xml из html»
         */
        'DIV'     => 'edit1',
        'TAB'     => Loc::getMessage('ORWO_SKSP_OPTIONS_TAB_GENERAL'),
        'TITLE'   => Loc::getMessage('ORWO_SKSP_OPTIONS_TAB_GENERAL'),
        'OPTIONS' => array(
            Loc::getMessage('ORWO_SKSP_OPTIONS_FILE_SETTINGS'),
            array(
                'sitemap_url',                                  
                Loc::getMessage('ORWO_SKSP_OPTIONS_SITEMAPURL'),
                '/sitemap/',                                           
                array('text')                             
            ),
            array(
                'xml_sitemap_name',
                Loc::getMessage('ORWO_SKSP_OPTIONS_XML_SITEMAP_NAME'),
                'sitemap-orwo.xml',                                          
                array('text')                              
            ),
            Loc::getMessage('ORWO_SKSP_OPTIONS_GENERATIONS_TYPE'),
            array(
                'static_html',
                Loc::getMessage('ORWO_SKSP_OPTIONS_STATIC_HTML'),
                'N',                                          
                array('checkbox')                              
            ),
            loc::getMessage('ORWO_SKSP_OPTIONS_TAB_SECOND_TITLE'),
            array(
                'contain_tag',
                Loc::getMessage('ORWO_SKSP_OPTIONS_CONTAIN_TAG'),
                'ul',
                array('text')
            ),
            array(
                'container_atribut',
                Loc::getMessage('ORWO_SKSP_OPTIONS_CONTAIN_ATTRIBUT'),
                '',
                array('text')
            ),
            array(
                'link_atribut',
                Loc::getMessage('ORWO_SKSP_OPTIONS_LINK_ATTRIBUT'),
                '',
                array('text')
            ),
            Loc::getMessage('ORWO_SKSP_OPTIONS_AGENT_GENERATE'),
            array(
                'agent_create',
                Loc::getMessage('ORWO_SKSP_OPTIONS_AGENT_CREATE'),
                'N',
                array('checkbox')
            ),
            array(
                'agent_time',
                Loc::getMessage('ORWO_SKSP_OPTIONS_AGENT_TIME'),
                '86400',
                array('text')
            )
        )
    ),
    array(
        /*
         * Вторая вкладка «Насторйка генерации html из xml»
         */
        'DIV'     => 'edit2',
        'TAB'     => Loc::getMessage('ORWO_SKSP_OPTIONS_TAB_SECOND'),
        'TITLE'   => '',
        'OPTIONS' => array(
            Loc::getMessage('ORWO_SKSP_OPTIONS_FILE_SETTINGS_SECOND_TAB'),
            array(
                'xml_sitemap_file',
                Loc::getMessage('ORWO_SKSP_OPTIONS_XML_SITEMAP_FILE_SECOND_TAB'),
                'sitemap.xml',
                array('text')
            ),
            array(
                'html_sitemap_file',
                Loc::getMessage('ORWO_SKSP_OPTIONS_HTML_SITEMAP_FILE_SECOND_TAB'),
                '/sitemap/',
                array('text')
            ),
            Loc::getMessage('ORWO_SKSP_OPTIONS_DOP_GENERATE_SETTINGS'),
            array(
                'create_html_file',
                Loc::getMessage('ORWO_SKSP_OPTIONS_HTML_FILE_CREATE'),
                'N',
                array('checkbox')
            ),
            Loc::getMessage('ORWO_SKSP_OPTIONS_AGENT_SETTINGS_SECOND_TAB'),
            array(
                'create_agent_xml_to_html',
                Loc::getMessage('ORWO_SKSP_OPTIONS_AGENT_CREATE_SECOND_TAB'),
                'N',
                array("checkbox")
            ),
            array(
                'html_agent_time',
                Loc::getMessage('ORWO_SKSP_OPTIONS_AGENT_INTERVAL_SECOND_TAB'),
                '86400',
                array('text')
            )
        )
    )
);

/*
 * Создаем форму для редактирвания параметров модуля
 */
echo '<div class="adm-info-message-wrap" >
                <div class="adm-info-message">
                    <p>'.Loc::getMessage("ORWO_SKSP_OPTIONS_DESC_TEXT").'</p>
                </div>
            </div>';
$tabControl = new CAdminTabControl(
    'tabControl',
    $aTabs
);

$tabControl->begin();
?>
<form action="<?=$APPLICATION->getCurPage(); ?>?mid=<?=$module_id; ?>&lang=<?= LANGUAGE_ID; ?>" method="post">
    <?= bitrix_sessid_post(); ?>
    <?php
    foreach ($aTabs as $aTab) { // цикл по вкладкам
        if ($aTab['OPTIONS']) {
            $tabControl->beginNextTab();
            __AdmSettingsDrawList($module_id, $aTab['OPTIONS']);
        }
    }
    $tabControl->buttons();
    ?>
    <input type="submit" name="apply" value="<?= Loc::GetMessage('ORWO_SKSP_OPTIONS_INPUT_APPLY'); ?>" class="adm-btn-save" />
    <input type="submit" name="to_go" value="<?= Loc::getMessage('ORWO_SKSP_OPTIONS_INPUT_TOGO')?>" />
    <input type="submit" name="xml_into_html" value="<?= Loc::getMessage('ORWO_SKSP_OPTIONS_INPUT_TOGO_XML_INTO_HTML')?>" />
</form>

<?php
$tabControl->end();

/*
 * Обрабатываем данные после отправки формы
 */
if ($request->isPost() && check_bitrix_sessid()) {

    foreach ($aTabs as $aTab) { // цикл по вкладкам
        foreach ($aTab['OPTIONS'] as $arOption) {
            if (!is_array($arOption)) { // если это название секции
                continue;
            }
            if ($arOption['note']) { // если это примечание
                continue;
            }
            if ($request['apply'] || $request['to_go'] || $request['xml_into_html']) { // сохраняем введенные настройки
                $optionValue = $request->getPost($arOption[0]);
                if ($arOption[0] == 'static_html') {
                    if ($optionValue == '') {
                        $optionValue = 'N';
                    }
                }
                Option::set($module_id, $arOption[0], is_array($optionValue) ? implode(',', $optionValue) : $optionValue);
            }
        }
    }
    if ($request['to_go']) { // устанавливаем по умолчанию
        $cl = new \Sksp\Module\SitemapParser;
        $cl->parseAndWrite();
    }
    if($request["agent_create"]=="Y"){
        $cl = new \Sksp\Module\SitemapParser;
        $cl->agentCreate($request["agent_time"]);
    }
    if($request['xml_into_html']){
        $htmlIntoXml = new \Sksp\Module\XmlSitemapIntoHtml;
        $htmlIntoXml->parseAndWrite();
    }
    if($request['create_agent_xml_to_html']){
        $cl2 = new \Sksp\Module\XmlSitemapIntoHtml;
        $cl2->agentCreate($request["html_agent_time"]);
    }

    LocalRedirect($APPLICATION->getCurPage().'?mid='.$module_id.'&lang='.LANGUAGE_ID);

}
?>