<?php
/*
 * Файл local/modules/scrollup/lang/ru/options.php
 */

$MESS['ORWO_SKSP_OPTIONS_TAB_GENERAL']    = 'Настройки генерации xml из html'; 
$MESS["ORWO_SKSP_OPTIONS_SITEMAPURL"]     = 'Укажите ссылку на html карты сайта от корня, например <b>/sitemap/</b>';
$MESS["ORWO_SKSP_OPTIONS_XML_SITEMAP_NAME"]    = 'Напишите название для .xml файла';
$MESS["ORWO_SKSP_OPTIONS_STATIC_HTML"]    = 'Карта сайта представляет из-себя статичную html верстку? (<b>НЕ испоьзуются компоненты</b>)';
$MESS["ORWO_SKSP_OPTIONS_AGENT_CREATE"]    = 'Обновлять xml карту автоматически? (будет создан агент)';
$MESS["ORWO_SKSP_OPTIONS_AGENT_TIME"]    = 'Укажите интервал обновления в секундах';
$MESS["ORWO_SKSP_OPTIONS_TAB_SECOND"]    = 'Насторйка генерации html из xml';
$MESS["ORWO_SKSP_OPTIONS_TAB_SECOND_TITLE"]    = "Настройка атрибутов основных элементов ( настройки на этой влкадке выполняются, если карта сайта выводится динамически (<b>компонентами</b>) )";
$MESS["ORWO_SKSP_OPTIONS_CONTAIN_TAG"]    = 'Тэг родительского контейнера, например div или ul';
$MESS["ORWO_SKSP_OPTIONS_CONTAIN_ATTRIBUT"]    = 'Класс (<b>.class</b>) или id (<b>#id</b>) родительского контейнера карты сайта';
$MESS["ORWO_SKSP_OPTIONS_LINK_ATTRIBUT"]    = 'Класс (<b>.class</b>) или id (<b>#id</b>) элемента "&lt;a&gt;" карты сайта, который содержит ссылки (не обязательно)';
$MESS["ORWO_SKSP_OPTIONS_DESC_TEXT"]      = 'Модуль добавил компонент orwo:html_sitemap - html карты сайта, который выстраивает карту из выбранных инфоблоков. Создайте страницу для вашей карты сайта и разместите на ней компонент. Если у вас уже есть страница с картой сайта, то устанавливать компонент на ней не обязательно. Далее нужно выпонлить настройки, которые позволят собрать xml карту сайта из вашей html карты.';
$MESS['ORWO_SKSP_OPTIONS_INPUT_APPLY']    = 'Сохранить настройки';
$MESS['ORWO_SKSP_OPTIONS_INPUT_TOGO']    = 'Запустить генерацию xml карты из html';
$MESS['ORWO_SKSP_OPTIONS_FILE_SETTINGS']  = 'Настройка файлов';
$MESS['ORWO_SKSP_OPTIONS_GENERATIONS_TYPE'] = 'Настройка типа генерации';
$MESS['ORWO_SKSP_OPTIONS_AGENT_GENERATE']   = 'Настройка генерации агента';
$MESS['ORWO_SKSP_OPTIONS_FILE_SETTINGS_SECOND_TAB']   = 'Настройка путей файлов';
$MESS['ORWO_SKSP_OPTIONS_XML_SITEMAP_FILE_SECOND_TAB'] = 'Укажите название файла xml карты сайта';
$MESS['ORWO_SKSP_OPTIONS_HTML_SITEMAP_FILE_SECOND_TAB'] = 'Укажите путь до html карты сайта (файл может быть формата .php)';
$MESS['ORWO_SKSP_OPTIONS_DOP_GENERATE_SETTINGS']  = 'Дополнительные настройки генерации';
$MESS['ORWO_SKSP_OPTIONS_HTML_FILE_CREATE']  = 'Создать раздел и файл для html карты сайта? (отметьте, если страница карты сайта еще не создана, она будет создана по пути указонному в настройке выше)';
$MESS['ORWO_SKSP_OPTIONS_XML_TO_XML'] = 'Карта xml содержит ссылки на другие xml файлы';
$MESS['ORWO_SKSP_OPTIONS_AGENT_SETTINGS_SECOND_TAB'] = 'Настройка генерации агента';
$MESS['ORWO_SKSP_OPTIONS_AGENT_CREATE_SECOND_TAB'] = 'Обновлять автоматически? (будет создан агент)';
$MESS['ORWO_SKSP_OPTIONS_AGENT_INTERVAL_SECOND_TAB'] = 'Укажите интервал обновления в секундах';
$MESS['ORWO_SKSP_OPTIONS_INPUT_TOGO_XML_INTO_HTML']  = 'Запустить генерацию html карты из xml';