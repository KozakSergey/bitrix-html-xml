<?php

namespace Sksp\Module;

use Bitrix\Main\Config\Option;
use Bitrix\Main\Page\Asset;
use Bitrix\Main\Application;
use Bitrix\Main\Web\Uri;
use Bitrix\Main\Context;
use Bitrix\Main\HttpRequest;
class XmlSitemapIntoHtml
{
    const MODULE_ID = "sksp.module";
    public $SITEMAP_XML;
    public $SITEMAP_HTML;
    public $HTML_FOLDER_CREATE;
    public function __construct(){
        $this->SITEMAP_XML = Option::get(self::MODULE_ID, "xml_sitemap_file");
        $this->SITEMAP_HTML = Option::get(self::MODULE_ID, "html_sitemap_file");
        $this->HTML_FOLDER_CREATE = Option::get(self::MODULE_ID, "create_html_file");
    }

    public function helloWorld(){
        $file2 = fopen($_SERVER["DOCUMENT_ROOT"].'/id.txt','a');
        fwrite($file2, $this->HTML_STATIC."");
        fclose($file2);
        echo "HELLO";
    }

    public function parseAndWrite(){
        $urls = self::extract_urls(simplexml_load_file($_SERVER["DOCUMENT_ROOT"].$this->SITEMAP_XML));
        $urls = array_unique($urls); // remove duplicates
        if($this->HTML_FOLDER_CREATE=="Y"){
            if(preg_match("/\/(.+?)\//",$this->SITEMAP_HTML, $match)){
                if(!is_dir($_SERVER["DOCUMENT_ROOT"].$match[0])){
                    $prava = '777';
                    $dir = mkdir( rtrim( $_SERVER['DOCUMENT_ROOT'], DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR.$match[0], $prava);
                }
            }
            if(!preg_match("/\.php/", $this->SITEMAP_HTML) && !preg_match("/\.htm/",$this->SITEMAP_HTML)){
                $newFile = fopen($_SERVER["DOCUMENT_ROOT"].$this->SITEMAP_HTML."index.php","w");
            }else{
                $newFile = fopen($_SERVER["DOCUMENT_ROOT"].$this->SITEMAP_HTML,"w");
            }
            if(preg_match("/\.php/",$this->SITEMAP_HTML)){
                $text = '<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");'."\n";
                $text .= '$APPLICATION->SetTitle("Карта сайта");?>'."\n";
                $text .= "\n\n";
                $text .= '<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>';
            }elseif(preg_match("/\.htm/",$this->SITEMAP_HTML)){
                $text = "<!DOCTYPE html>\n";
                $text .= "<html>\n";
                $text .= "\t<head>\n";
                $text .= "\t\t<meta charset='utf-8'>\n";
                $text .= "\t\t<meta name='viewport' content='width=device-width, initial-scale=1'>\n";
                $text .= "\t\t<title>Карта сайта</title>\n";
                $text .= "\t</head>\n";
                $text .= "\t<body>\n";
                $text .= "\n\n";
                $text .= "\t</body></html>";
            }else{
                $text = '<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");'."\n";
                $text .= '$APPLICATION->SetTitle("Карта сайта");?>'."\n";
                $text .= "\n\n";
                $text .= '<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>';
            }
            fwrite($newFile, $text);
            fclose($newFile);
        }
        if(!preg_match("/\.php/", $this->SITEMAP_HTML) && !preg_match("/\.htm/",$this->SITEMAP_HTML)){
            $fileSitemap = $this->SITEMAP_HTML."index.php";
        }else{
            $fileSitemap = $this->SITEMAP_HTML;
        }
        $html = "\t<ul class='sitemap-contain'>\n";
        foreach ($urls as $url) {
            $pageH1 = self::getH1($url);
            if($pageH1==false){continue;}
            $html .= "\t\t<li><a href='$url'>$pageH1</a></li>\n";
        }
        $html .= "\t</ul>\n";
        if($this->HTML_FOLDER_CREATE=="Y"){
            $file_contents = file($_SERVER["DOCUMENT_ROOT"].$fileSitemap);
            $php_end = array_pop($file_contents);
            array_splice($file_contents, count($file_contents), 0, $html);
            array_push($file_contents, $php_end);
            file_put_contents($_SERVER["DOCUMENT_ROOT"].$fileSitemap, $file_contents);
        }else{
            $file_contents = file_get_contents($_SERVER["DOCUMENT_ROOT"].$fileSitemap);
            preg_match("#<ul class='sitemap-contain'>(.+?)</ul>#su",$file_contents,$links);
            if(!empty($links[0])){
                $file_contents = str_replace($links[0],$html,$file_contents);
            }else{
                $file_contents = file($_SERVER["DOCUMENT_ROOT"].$fileSitemap);
                $php_end = array_pop($file_contents);
                array_splice($file_contents, count($file_contents), 0, $html);
                array_push($file_contents, $php_end);
            }
            file_put_contents($_SERVER["DOCUMENT_ROOT"].$fileSitemap, $file_contents);
        }

    }

    public function getH1($link){
        $agent= 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_USERAGENT, $agent);
        curl_setopt($ch, CURLOPT_URL,$link);
        $conetent = curl_exec($ch);
        preg_match("#<h1>(.+?)</h1>#su",$conetent,$match);
        if(!empty($match[0])){
            $title = $match[0];
        }else{
            $title = $match[1];
        }
        $result = str_replace("<h1>","",$title);
        $result = str_replace("</h1>","",$result);
        return $result;
    }
    public function extract_urls($xml)
    {
        $urls = array();
        foreach ($xml->url as $url) {
            array_push($urls, (string) $url->loc);
        }

        foreach ($xml->sitemap as $sitemap) {
            $loc = (string) $sitemap->loc;
            if (substr($loc, -4) === '.xml') {
                $urls = array_merge($urls,
                    self::extract_urls(simplexml_load_file($loc)));
            }
        }

        return $urls;
    }

    public function parseAndWriteForAgent($sitemapXml, $sitemapHtml){
        $urls = self::extract_urls(simplexml_load_file($_SERVER["DOCUMENT_ROOT"].$sitemapXml));
        $urls = array_unique($urls);
        if(!preg_match("/\.php/", $sitemapHtml) && !preg_match("/\.htm/",$sitemapHtml)){
            $fileSitemap = $sitemapHtml."index.php";
        }else{
            $fileSitemap = $sitemapHtml;
        }
        $html = "\t<ul class='sitemap-contain'>\n";
        foreach ($urls as $url) {
            $pageH1 = self::getH1($url);
            if($pageH1==false){continue;}
            $html .= "\t\t<li><a href='$url'>$pageH1</a></li>\n";
        }
        $html .= "\t</ul>\n";
        $file_contents = file_get_contents($_SERVER["DOCUMENT_ROOT"].$fileSitemap);
        preg_match("#<ul class='sitemap-contain'>(.+?)</ul>#su",$file_contents,$links);
        if(!empty($links[0])){
            $file_contents = str_replace($links[0],$html,$file_contents);
            file_put_contents($_SERVER["DOCUMENT_ROOT"].$fileSitemap, $file_contents);
            return '\Sksp\Module\XmlSitemapIntoHtml::parseAndWriteForAgent("'.$sitemapXml.'","'.$sitemapHtml.'");';
        }else{
            return '\Sksp\Module\XmlSitemapIntoHtml::parseAndWriteForAgent(false);';
        }
    }

    public function agentCreate($time)
    {
        \CAgent::RemoveAgent("\Sksp\Module\XmlSitemapIntoHtml::parseAndWriteForAgent('$this->SITEMAP_XML', '$this->SITEMAP_HTML');", "sksp.module");
        \CAgent::AddAgent(
            "\Sksp\Module\XmlSitemapIntoHtml::parseAndWriteForAgent('$this->SITEMAP_XML', '$this->SITEMAP_HTML');",
            "sksp.module",
            "N",                                  // агент не критичен к кол-ву запусков
            $time,                                // интервал запуска - 1 сутки
            "",                // дата первой проверки на запуск
            "Y",                                  // агент активен
            "",                // дата первого запуска
            100);
    }
}