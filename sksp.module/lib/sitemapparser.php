<?php
namespace Sksp\Module;

use Bitrix\Main\Config\Option;
use Bitrix\Main\Page\Asset;
use Bitrix\Main\Application;
use Bitrix\Main\Web\Uri;
use Bitrix\Main\Context;
use Bitrix\Main\HttpRequest;

class SitemapParser {

    const MODULE_ID = "sksp.module";
    public $PATH_MAP;
    public $FILE_XML;
    public $ID_CONTAINER;
    public $ID_LINK;
    public $HTML_STATIC;
    public $HTML_TAG;
    public $protocol;
    public $domain;

    public function __construct()
    {
        $this->PATH_MAP = Option::get(self::MODULE_ID, "sitemap_url");
        $this->FILE_XML = Option::get(self::MODULE_ID, "xml_sitemap_name");
        $this->ID_CONTAINER = Option::get(self::MODULE_ID, "container_atribut");
        $this->ID_LINK = Option::get(self::MODULE_ID, "link_atribut");
        $this->HTML_STATIC = Option::get(self::MODULE_ID, "static_html");
        $this->HTML_TAG = Option::get(self::MODULE_ID, "contain_tag");
        $this->protocol = ($_SERVER['SERVER_PORT'] === 443) ? 'https://' : 'http://';
        $this->domain = $_SERVER['HTTP_HOST'];
    }


    public function parseAndWrite(){

        if($this->HTML_STATIC=="Y"){
            if(!preg_match("/\.php/", $this->PATH_MAP)){
                $fileSitemap = $this->PATH_MAP."index.php";
            }else{
                $fileSitemap = $this->PATH_MAP;
            }
            $html = file_get_contents($_SERVER["DOCUMENT_ROOT"].$fileSitemap);
            $dom = new \DOMDocument;
            libxml_use_internal_errors(true);
            $dom->loadHTML($html);
            libxml_clear_errors();
            $xpath = new \DOMXPath($dom);
            $links = $dom->getElementsByTagName('a');
            $urls = [];
            foreach($links as $link) {
                $url = $link->getAttribute('href');
                $urls[] = $url;
            }
        }else{
            $html = file_get_contents($this->protocol.$this->domain.$this->PATH_MAP."");
            $dom = new \DOMDocument;
            libxml_use_internal_errors(true);
            $dom->loadHTML($html);
            libxml_clear_errors();
            $xpath = new \DOMXpath($dom);
            $classElem = "a_link";
            if(preg_match("/\./",$this->ID_CONTAINER)){
                $attrContain = "class";
                $classCont = str_replace(".","",$this->ID_CONTAINER);
            }elseif(preg_match("/\#/",$this->ID_CONTAINER)){
                $attrContain = "id";
                $classCont = str_replace("#","",$this->ID_CONTAINER);
            }
            if($this->ID_LINK){
                if(preg_match("/\./",$this->ID_LINK)){
                    $attrElem = "class";
                    $classElem = str_replace(".","",$this->ID_LINK);
                }elseif(preg_match("/\3/",$this->ID_LINK)){
                    $attrElem = "id";
                    $classElem = str_replace("#","",$this->ID_LINK);
                }
                /*тут по примеру условия выше выводим класс или ади элемента А, а потом переменные классов или айди преобразуем в слова без точек и решоток.И добавляем в условие ниже.*/
                $links = $xpath->query('//'.$this->HTML_TAG.'[@'.$attrContain.'="'.$classCont.'"]//a[contains(concat(" ", normalize-space(@'.$attrElem.'), " "), " '.$classElem.' ")]');
            }else{
                $links = $xpath->query('//'.$this->HTML_TAG.'[@'.$attrContain.'="'.$classCont.'"]//a');
            }
            $urls = [];
            foreach($links as $link) {
                $url = $link->getAttribute('href');
                $urls[] = $url;

            }
        }

        self::writeToXml($urls,$this->FILE_XML,$this->protocol,$this->domain);
    }

    public function writeToXml($data,$fileName,$curProtocol,$curDomain)
    {
        $xml = new \DOMDocument('1.0', 'UTF-8');
        $xmlUrlset = $xml->createElement('urlset');
        $xmlUrlset->setAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');
        $xml->appendChild($xmlUrlset);

        foreach ($data as $loc)
        {
            if(!preg_match("/http/",$loc)){
                $loc = $curProtocol.$curDomain.$loc;
            }
            $xmlUrl = $xml->createElement('url');
            $xmlLoc = $xml->createElement('loc', htmlspecialchars($loc));
            $xmlUrl->appendChild($xmlLoc);
            $xmlUrlset->appendChild($xmlUrl);
        }

        $xml->formatOutput = true;
        $xml->save($_SERVER["DOCUMENT_ROOT"]."/".$fileName);
    }
    public function parseAndWriteWithParams($htmlStatic,$pathMap,$protocol,$curDomain,$contain_attr,$elem_attr,$tagHtm,$xmlFile){
        if($htmlStatic=="Y"){
            if(!preg_match("/\.php/", $pathMap)){
                $fileSitemap = $pathMap."index.php";
            }else{
                $fileSitemap = $pathMap;
            }
            $html = file_get_contents($_SERVER["DOCUMENT_ROOT"].$fileSitemap);
            $dom = new \DOMDocument;
            libxml_use_internal_errors(true);
            $dom->loadHTML($html);
            libxml_clear_errors();
            $xpath = new \DOMXPath($dom);
            $links = $dom->getElementsByTagName('a');
            $urls = [];
            foreach($links as $link) {
                $url = $link->getAttribute('href');
                $urls[] = $url;
            }
        }else{
            $html = file_get_contents($protocol.$curDomain.$pathMap."");
            $dom = new \DOMDocument;
            libxml_use_internal_errors(true);
            $dom->loadHTML($html);
            libxml_clear_errors();
            $xpath = new \DOMXpath($dom);
            $classElem = "a_link";
            if(preg_match("/\./",$contain_attr)){
                $attrContain = "class";
                $classCont = str_replace(".","",$contain_attr);
            }elseif(preg_match("/\#/",$contain_attr)){
                $attrContain = "id";
                $classCont = str_replace("#","",$contain_attr);
            }
            if($elem_attr){
                if(preg_match("/\./",$elem_attr)){
                    $attrElem = "class";
                    $classElem = str_replace(".","",$elem_attr);
                }elseif(preg_match("/\#/",$elem_attr)){
                    $attrElem = "id";
                    $classElem = str_replace("#","",$elem_attr);
                }
                $links = $xpath->query('//'.$tagHtm.'[@'.$attrContain.'="'.$classCont.'"]//a[contains(concat(" ", normalize-space(@'.$attrElem.'), " "), " '.$classElem.' ")]');
            }else{
                $links = $xpath->query('//'.$tagHtm.'[@'.$attrContain.'="'.$classCont.'"]//a');
            }
            $urls = [];
            foreach($links as $link) {
                $url = $link->getAttribute('href');
                $urls[] = $url;

            }
        }

        self::writeToXml($urls,$xmlFile,$protocol,$curDomain);
        return '\Sksp\Module\SitemapParser::parseAndWriteWithParams("'.$htmlStatic.'","'.$pathMap.'","'.$protocol.'","'.$curDomain.'","'.$contain_attr.'","'.$elem_attr.'","'.$tagHtm.'","'.$xmlFile.'");';
    }
    public function agentCreate($time)
    {
        \CAgent::RemoveAgent("\Sksp\Module\SitemapParser::parseAndWriteWithParams('$this->HTML_STATIC','$this->PATH_MAP','$this->protocol','$this->domain','$this->ID_CONTAINER','$this->ID_LINK','$this->HTML_TAG','$this->FILE_XML');", "sksp.module");
        \CAgent::AddAgent(
            "\Sksp\Module\SitemapParser::parseAndWriteWithParams('$this->HTML_STATIC','$this->PATH_MAP','$this->protocol','$this->domain','$this->ID_CONTAINER','$this->ID_LINK','$this->HTML_TAG','$this->FILE_XML');",
            "sksp.module",
            "N",                                  // агент не критичен к кол-ву запусков
            $time,                                // интервал запуска - 1 сутки
            "",                // дата первой проверки на запуск
            "Y",                                  // агент активен
            "",                // дата первого запуска
            100);
    }
}
